# README #

This is a working repository for OAuth 2.0 JWT Authorization Request, 
which is a working group item at IETF OAuth WG. 

We started this repo before IETF started using git so... 

### What is this repository for? ###

* OAuth 2.0 JWT Authorization Request
* [OAuth JAR](https://tools.ietf.org/html/draft-ietf-oauth-jwsreq)

### Which file should I look at?  ###

* draft-oauth-jwsreq.xml is the master file. All other .xml files are for historical purpose so do not edit. 

### Contribution guidelines ###

* You have to agree to IETF rules. 
* Compile and check. 
* Post the proposed change for the discussion in the IETF list. 

### Who do I talk to? ###

* oauth@ietf.org